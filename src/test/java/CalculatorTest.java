import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Description;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import steps.CalculatorSteps;

import java.util.List;

import static com.codeborne.selenide.Selenide.open;

public class CalculatorTest {

    private CalculatorSteps calculatorSteps = new CalculatorSteps();

    @BeforeAll
    static void setup() {
        Configuration.timeout = 10000;
        Configuration.remote = System.getProperty("selenideRemote", null);
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
        open("http://web2.0calc.com/");
    }

    @Test
    @Description("Calculator history test")
    public void checkCalculatorHistory() {
        calculatorSteps.acceptConsent();

        calculatorSteps.checkCalculatingExpressionCorrect("35*999+(100/4)", "34990");

        calculatorSteps.chooseRadOption();
        calculatorSteps.checkCalculatingExpressionCorrect("cos(pi)", "-1");

        calculatorSteps.checkCalculatingExpressionCorrect("sqrt(81)", "9");

        calculatorSteps.checkHistoryExpressionList(List.of(
                "sqrt(81)",
                "cos(pi)",
                "35*999+(100/4)"
        ));
    }

    @AfterAll
    static void tearDown() {
        WebDriverRunner.closeWebDriver();
    }
}
