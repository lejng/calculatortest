package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import components.HistoryDropdown;

import java.time.Duration;
import java.util.List;

import static com.codeborne.selenide.ClickOptions.usingJavaScript;
import static com.codeborne.selenide.Selenide.*;

public class CalculatorPage {

    private HistoryDropdown historyDropdown = new HistoryDropdown();

    private SelenideElement calculatorInput = $("#input");

    private SelenideElement equalsButton = $("#BtnCalc");

    private SelenideElement acceptConsentButton = $x("//button[@aria-label='Consent']");

    private SelenideElement clearButton = $("#BtnClear");

    private SelenideElement radRadioButton = $("#trigorad");

    public void enterExpression(String expression) {
        calculatorInput.setValue(expression);
    }

    public void calculate() {
        equalsButton.click();
    }

    public void checkResult(String expectedResult) {
        calculatorInput.shouldHave(Condition.value(expectedResult), Duration.ofSeconds(5));
    }

    public void clearInput() {
        clearButton.click();
    }

    public void acceptConsentIfNeed() {
        if(acceptConsentButton.isDisplayed()) {
            acceptConsentButton.click(usingJavaScript());
        }
    }

    public void chooseRadOption() {
        if(!radRadioButton.isSelected()) {
            radRadioButton.click();
        }
    }

    public List<String> getHistoryExpressionList() {
        historyDropdown.open();
        return historyDropdown.getExpressionList();
    }
}
