package steps;

import io.qameta.allure.Step;
import pages.CalculatorPage;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorSteps {

    private CalculatorPage calculatorPage = new CalculatorPage();

    @Step("Following expression '{expression}' must be have result '{expectedResult}' on Calculator page")
    public void checkCalculatingExpressionCorrect(String expression, String expectedResult) {
        calculatorPage.enterExpression(expression);
        calculatorPage.calculate();
        calculatorPage.checkResult(expectedResult);
        calculatorPage.clearInput();
    }

    @Step("History expression list must be have following values: '{expectedHistoryExpressionList}' on Calculator page")
    public void checkHistoryExpressionList(List<String> expectedHistoryExpressionList) {
        List<String> actualHistoryExpressionList = calculatorPage.getHistoryExpressionList();
        assertEquals(expectedHistoryExpressionList, actualHistoryExpressionList, "Incorrect expression history of calculator");
    }

    @Step("Choosing rad option on Calculator page")
    public void chooseRadOption() {
        calculatorPage.chooseRadOption();
    }

    @Step("Clicking accept consent on Calculator page")
    public void acceptConsent() {
        calculatorPage.acceptConsentIfNeed();
    }
}
