package components;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Objects;

import static com.codeborne.selenide.Selenide.*;
import static java.util.stream.Collectors.toList;

public class HistoryDropdown {

    private SelenideElement historyDropdownElement = $("#hist");

    private SelenideElement openButton = historyDropdownElement.find(By.xpath(".//button[contains(@class,'toggle')]"));

    private ElementsCollection expressions = $$x("//*[@id='histframe']//li//p[2]");

    public void open() {
        if(!isOpen()) {
            openButton.click();
        }
    }

    public boolean isOpen() {
        return Objects.requireNonNull(historyDropdownElement.getAttribute("class")).contains("open");
    }

    public List<String> getExpressionList() {
        return expressions
                .stream()
                .map(SelenideElement::text)
                .collect(toList());
    }
}
