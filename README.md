# Web calculator test

1. Support java 11 or above
2. Execute tests ```./gradlew runTests```
3. Execute tests and generated allure report ```./gradlew runTests allureReport``` report will be here ```\build\allure-report\allureReport```
4. Link to allure gradle plugin ```https://github.com/allure-framework/allure-gradle```
5. Run tests in docker ```docker-compose up --abort-on-container-exit ``` and after finish ```docker-compose down```, to see what is happening inside the container, head to http://localhost:7900 (password is secret), report will be here ```\build\allure-report\allureReport```